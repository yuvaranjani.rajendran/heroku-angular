import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Click on a coin to increment the value of the bank.');
  });

  it('should increment 5 cent for every dime click', () => {
    page.navigateTo();
    page.getNickel().click();
    expect(page.getTotal()).toEqual('$0.05');
  });

  it('should increment 25 cents for every quarter click', () => {
    page.navigateTo();
    page.getQuarter().click();
    expect(page.getTotal()).toEqual('$0.25');
  });

  it('should progressively sum totals with multiple clicks', () => {
    page.navigateTo();
    page.getQuarter().click();
    page.getQuarter().click();
    page.getQuarter().click();
    page.getNickel().click();
    expect(page.getTotal()).toEqual('$0.80');
  });



  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
