import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.css('app-root .content span')).getText() as Promise<string>;
  }

  getTotal(): Promise<string> {
    return element(by.css('.bank .total')).getText() as Promise<string>;
  }

  getNickel()  {
    return element(by.css('.nickel'));
  }

  getQuarter()  {
    return element(by.css('.quarter'));
  }
}
